Introduction
------------

IoT Sensor could generate lots of data in short period of time. Sometimes it is useful to represent this data in 
user friendly graphical way.

If your IoT devices are connected to MQTT Broker, then it is simple to write a script (JavaScript) to connect to the same
MQTT Broker and wait for data to arrive. When data is received you can update web graph and get near real time representation
of sensors state.

### Real Time Graph for MQTT Sensor Data

After some experimenting with ELK stack (Elasticsearch, Logstash, Kibana), Redis and other tools...

The most simple setup is like this:

IoT Sensor -> MQTT Broker (Mosquitto) -> JavaScript MQTT Client + Data Visualisation

Very [simple and useful](http://smoothiecharts.org/) JavaScript library for real time data visualisation is Smoothie.js

Here is real time graph from Arduino sensor measuring Temp and Humidity.

![image](https://bitbucket.org/etailerrs/iot-real-time-graph/raw/7e2053fb31b103cc9e87ebf5e339353040d1a941/sample.png)